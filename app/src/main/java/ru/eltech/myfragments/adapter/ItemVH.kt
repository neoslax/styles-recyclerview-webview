package ru.eltech.myfragments.adapter

import androidx.recyclerview.widget.RecyclerView
import ru.eltech.myfragments.databinding.ItemBinding

class ItemVH(val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root)