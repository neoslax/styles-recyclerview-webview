package ru.eltech.myfragments.adapter

data class Item(
    val name: String,
    val count: Int
)
