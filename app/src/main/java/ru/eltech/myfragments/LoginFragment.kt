package ru.eltech.myfragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ru.eltech.myfragments.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var bottomBarEnabler: BottomBarEnabler

    private var _binding: FragmentLoginBinding? = null
    private val binding: FragmentLoginBinding
        get() = _binding ?: throw RuntimeException("FragmentLoginBinding == null")

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BottomBarEnabler) {
            bottomBarEnabler = context
        } else throw RuntimeException("Activity must implement BottomBarEnabler")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvLoginToRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        binding.btnLogin.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            bottomBarEnabler.setBottomBarVisible()

        }
    }

    interface BottomBarEnabler {
        fun setBottomBarVisible()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}